package com.revoex.simple_bluetooth_le_terminal;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TerminalFragment extends Fragment implements ServiceConnection, SerialListener {

    private enum Connected {False, Pending, True}

    private String deviceAddress, lastPass, lockID, roomNo, hotelName;
    private String newline = "\r\n";
    private TextView receiveText;
    private SerialService service;
    private boolean initialStart = true;
    private Connected connected = Connected.False;
    private static boolean passKeyUpdatedSuccessfully = false;
    private static SpannableStringBuilder spn;

    /*
     * Lifecycle
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        deviceAddress = getArguments().getString("device");
        lockID = getArguments().getString("kitID");
        roomNo = getArguments().getString("roomNo");
        spn = new SpannableStringBuilder();
    }

    @Override
    public void onDestroy() {
        if (connected != Connected.False)
            disconnect();
        getActivity().stopService(new Intent(getActivity(), SerialService.class));
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (service != null)
            service.attach(this);
        else
            getActivity().startService(new Intent(getActivity(), SerialService.class)); // prevents service destroy on unbind from recreated activity caused by orientation change
    }

    @Override
    public void onStop() {
        if (service != null && !getActivity().isChangingConfigurations())
            service.detach();
        super.onStop();
    }

    @SuppressWarnings("deprecation")
    // onAttach(context) was added with API 23. onAttach(activity) works for all API versions
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getActivity().bindService(new Intent(getActivity(), SerialService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDetach() {
        try {
            getActivity().unbindService(this);
        } catch (Exception ignored) {
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (initialStart && service != null) {
            initialStart = false;
            getActivity().runOnUiThread(this::connect);
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        service = ((SerialService.SerialBinder) binder).getService();
        service.attach(this);
        if (initialStart && isResumed()) {
            initialStart = false;
            getActivity().runOnUiThread(this::connect);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        service = null;
    }

    /*
     * UI
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terminal, container, false);
        receiveText = view.findViewById(R.id.receive_text);                          // TextView performance decreases with number of spans
        receiveText.setTextColor(getResources().getColor(R.color.LimeGreen)); // set as default color to reduce number of spans
        receiveText.setMovementMethod(ScrollingMovementMethod.getInstance());
        TextView sendText = view.findViewById(R.id.send_text);
        View sendBtn = view.findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(v -> send(sendText.getText().toString()));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_terminal, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.retry) {
            //receiveText.setText("");
            disconnect();
            onStart();
            connect();
            return true;
        } else if (id == R.id.newline) {
            String[] newlineNames = getResources().getStringArray(R.array.newline_names);
            String[] newlineValues = getResources().getStringArray(R.array.newline_values);
            int pos = java.util.Arrays.asList(newlineValues).indexOf(newline);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Newline");
            builder.setSingleChoiceItems(newlineNames, pos, (dialog, item1) -> {
                newline = newlineValues[item1];
                dialog.dismiss();
            });
            builder.create().show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /*
     * Serial + UI
     */
    private void connect() {
        try {
            //spn.clearSpans();
            //spn.clear();
            Date date = new java.util.Date();
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceAddress);
            spn.clear();
            spn.append(DateFormat.getDateTimeInstance().format(date) + "\n\u2933 Connecting..." + '\n');

            spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.Blue)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            receiveText.append(spn);
            status("\u2933 To Lock ID #" + lockID + "; Room No: " + roomNo);
            connected = Connected.Pending;
            SerialSocket socket = new SerialSocket(getActivity().getApplicationContext(), device);
            service.connect(socket);
        } catch (Exception e) {
            onSerialConnectError(e);
        }
    }

    private void disconnect() {
        connected = Connected.False;
        service.disconnect();
    }

    private void send(String str) {
        if (connected != Connected.True) {
            Toast.makeText(getActivity(), "\u2933 Not Connected", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            SpannableStringBuilder spn = new SpannableStringBuilder("\u2933 " + str + '\n');
            spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.MediumSlateBlue)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            receiveText.append(spn);
            Log.d("NewPassKey", str);
            boolean cond = initiatePasswordChange(str);
            if (cond) {
                spn = new SpannableStringBuilder("\u2933 Password Updated on DB..." + '\n');
                spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.Fuchsia)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                receiveText.append(spn);
                byte[] data = ("2222" + str.replace(" ", "").replace("\0", "").replace("\r", "").replace("\n", "").trim() + "123456").getBytes();
                service.write(data);
            } else {
                spn = new SpannableStringBuilder("\u2933 Failed to reach to server. Seems network issue...Aborting" + '\n');
                spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.Chocolate)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                receiveText.append(spn);
            }
            passKeyUpdatedSuccessfully = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void receive(byte[] data) {
        String str;
        SpannableStringBuilder spn = new SpannableStringBuilder();
        String deviceResponse = new String(data);
        if (deviceResponse.trim().contains("K")) {
            str = "\u2933 Update Status: Updated Lock";
            spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.SteelBlue)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            str = "\u2933 Update Status: Failed to update lock...Please try again";
            spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.IndianRed)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        str = str + " (" + deviceResponse.trim() + ")";
        spn.append(str);
        receiveText.append(spn);
        disconnect();
        str = "\u2933 Disconnecting from Lock";
        status("");
        status(str);
        status("");
        //getActivity().stopService(new Intent(getActivity(), SerialService.class));
    }

    private void status(String str) {
        SpannableStringBuilder spn = new SpannableStringBuilder(str + '\n');
        spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.Salmon)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (str.equalsIgnoreCase("Connected")) {
            spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.DeepSkyBlue)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        receiveText.append(spn);
    }

    /*
     * SerialListener
     */
    @Override
    public void onSerialConnect() {
        status("\u2933 Connected");
        connected = Connected.True;
        String newKey = generateRandomPassword(10);
        send(newKey);
    }

    @Override
    public void onSerialConnectError(Exception e) {
        status("Connection Failed: " + e.getMessage());
        disconnect();
    }

    @Override
    public void onSerialRead(byte[] data) {
        receive(data);
    }

    @Override
    public void onSerialIoError(Exception e) {
        status("");
        status("\u2933 Connection Lost: " + e.getMessage());
        disconnect();
    }

    public boolean initiatePasswordChange(String newPass) throws ExecutionException, InterruptedException {
        final Response[] response = new Response[1];
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    HttpUrl.Builder urlBuilder = HttpUrl.parse("https://aws-revoex.tech/BaseDir/APIs/mobileApp/v1/updateKey.php").newBuilder();
                    urlBuilder.addQueryParameter("kitID", lockID);
                    urlBuilder.addQueryParameter("lockPass", newPass);
                    String url = urlBuilder.build().toString();
                    Request request = new Request.Builder()
                            .url(url)
                            .build();
                    response[0] = client.newCall(request).execute();
                    Log.i("NewPassKeyRespnse", String.valueOf(response[0].code()));
                    if (String.valueOf(response[0].code()).equalsIgnoreCase("200")) {
                        passKeyUpdatedSuccessfully = true;
                    } else {
                        passKeyUpdatedSuccessfully = false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response[0].code();
            }

            @Override
            protected void onPostExecute(Integer responseCode) {
                super.onPostExecute(responseCode);
                //status("Completed Operation");
            }
        }.execute().get();
        return passKeyUpdatedSuccessfully;
    }

    // Function to generate random alpha-numeric password of specific length
    public static String generateRandomPassword(int len) {
        // ASCII range - alphanumeric (0-9, a-z, A-Z)
        final String chars = "AB0CD1EFG3HIJK4LMNOPQ5RSTUVW6XYZab7cdefgh8ijklm9nopqrs0tuvwxyz";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        // each iteration of loop choose a character randomly from the given ASCII range
        // and append it to StringBuilder instance

        for (int i = 0; i < len; i++) {
            int randomIndex = random.nextInt(chars.length());
            if(sb.toString().indexOf(chars.charAt(randomIndex))>=0) {
                i = i - 1;
                //Log.i("Passkey Partial",sb.toString());
            }
            else {
                sb.append(chars.charAt(randomIndex));
            }
        }
        Log.i("Passkey",sb.toString());
        return sb.toString();
    }
}
