package com.revoex.simple_bluetooth_le_terminal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Rooms implements Parcelable, Cloneable {
    private String hotelName;
    private String hotelId;
    private String roomNumber;
    private String kitId;
    private String btAddress;
    private String lastPass;

    public Rooms(String  hotelName, String  hotelId, String roomNumber, String kitId, String btAddress, String lastPass) {
        this.hotelName = hotelName;
        this.hotelId = hotelId;
        this.roomNumber = roomNumber;
        this.kitId = kitId;
        this.btAddress = btAddress;
        this.lastPass = lastPass;
    }

    protected Rooms(Parcel in) {
        hotelName = in.readString();
        hotelId = in.readString();
        roomNumber = in.readString();
        kitId = in.readString();
        btAddress = in.readString();
        lastPass = in.readString();
    }

    public static final Creator<Rooms> CREATOR = new Creator<Rooms>() {
        @Override
        public Rooms createFromParcel(Parcel in) {
            return new Rooms(in);
        }

        @Override
        public Rooms[] newArray(int size) {
            return new Rooms[size];
        }
    };

    @SerializedName("hotelName")
    public String  hotelName() {
        return hotelName;
    }

    @SerializedName("hotelId")
    public String  hotelId() {
        return hotelId;
    }

    @SerializedName("roomNumber")
    public String roomNumber() {
        return roomNumber;
    }

    @SerializedName("kitId")
    public String kitId() {
        return kitId;
    }

    @SerializedName("btAddress")
    public String btAddress() {
        return btAddress;
    }

    @SerializedName("lastPass")
    public String lastPass() {
        return lastPass;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(hotelName);
        parcel.writeString(hotelId);
        parcel.writeString(roomNumber);
        parcel.writeString(kitId);
        parcel.writeString(btAddress);
        parcel.writeString(lastPass);
    }
}