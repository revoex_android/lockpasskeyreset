package com.revoex.simple_bluetooth_le_terminal;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {
    String BASE_URL = "https://aws-revoex.tech/BaseDir/APIs/mobileApp/v1/";

    @GET("returnRooms.php")
    Call<List<Rooms>> getAllRooms();
}